import os

def continuar():
    print()
    input('presione una tecla para continuar ...')
    os.system('cls')
###############################################################################
def menu():
    print('1) Registrar Productos')
    print('2) Mostrar el listado de productos.')
    print('3) Mostrar los productos cuyo stock este en un intervalo')
    print('4) Sumar stock a productos ')
    print('5) Eliminar productos con stock 0. ')
    print('6) Salir')
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 6)):
        eleccion = int(input('Elija una opción: '))
    os.system('cls')
    return eleccion
################################################################################
def ingreso():
    n = int(input("Ingrese valor: "))
    while not(n>= 0) :
        print("ERROR")
        n = int(input("Ingrese un valor valido: "))
    return n
################################################################################
def registrarProductos(productos):
    print('Cargar Lista de Productos')
    codigo = -1
    while (codigo != 0):
        codigo = int(input('Codigo de producto (cero para finalizar): '))
        if codigo != 0: 
            if codigo not in productos:    
                descripcion = input('Descripcion del producto: ')
                print("PRECIO ")
                precio = ingreso()
                print("STOCK ")
                stock = ingreso()
                productos[codigo] = [descripcion,precio,stock]
                print('agregado correctamente')
            else:
                print('el producto ya existe')
    return productos
################################################################################
def mostrar(diccionario):
    print('Listado de Productos')
    for clave, pre in diccionario.items():
        print(clave, pre)
################################################################################
def mostrarRango(diccionario):
    print('3) Mostrar los productos cuyo stock este en un intervalo')
    ranA = ingreso()
    ranB = ingreso()
    print('Listado de Productos')
    for clave, pre in diccionario.items():
        if (pre[2] >= ranA) and (pre[2]<=ranB):
            print(clave, pre)
################################################################################
def sumarRango(diccionario):
    print("Sumar Stock")
    ranX = ingreso()
    print("Minino Stock")
    ranY = ingreso()
    print('Listado de Productos')
    for clave, pre in diccionario.items():
        if (pre[2]<ranY):
            pre[2]=pre[2]+ranX
            print("Se sumo ",ranX," de stock a: ",clave, pre)
################################################################################
def eliminar(diccionario):
    borra=1
    while borra!=0:  
        for clave, pre in diccionario.items():
            if (pre[2] == 0):
                borra = clave
            else:
                borra == 0
        if borra!=0:
            diccionario.pop(borra)
################################################################################
def salir():
    print('Fin del programa...')
opcion = 0
productos = {}
os.system('cls')
while (opcion != 6):
    opcion = menu()
    if opcion == 1:
        productos = registrarProductos(productos)
    elif opcion == 2:
        mostrar(productos)
    elif opcion == 3:
        mostrarRango(productos)
    elif opcion == 4:
        sumarRango(productos)
    elif opcion == 5:
        eliminar(productos)
    elif (opcion == 6):        
        salir()
    continuar()
